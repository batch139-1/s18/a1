let trainer = {
  name: `Naruto`,
  age: 21,
  Pokemon: [`Lucario`, `Absol`, `Snorlax`],
  Friends: {
    Konoha: [`Ichigo`, `Yugi`],
    Fracture: [`Reyna`, `Phoenix`],
  },

  talk: (talk = function () {
    console.log(`${this.Pokemon[0]}! I choose you!`);
  }),
};

console.log(trainer.name);
console.log(trainer.age);
console.log(trainer.Pokemon);
console.log(trainer.Pokemon[1]);
console.log(trainer.Friends);
console.log(trainer.Friends.Konoha);
console.log(trainer.Friends.Fracture[0]);
trainer.talk();

function createPokemon(name, lvl, hp) {
  this.name = name;
  this.level = lvl;
  this.health = hp * 2;
  this.attack = lvl * 3;

  this.tackle = function (target) {
    console.log(`${this.name} tackled ${target.name}!`);
    let newHealth = target.health - this.attack;
    console.log(`${target.name}'s health has been reduced to ${newHealth}!`);
    if (newHealth <= 0) {
      this.faint(target);
    }
  };

  this.faint = function (target) {
    console.log(`${target.name} has fainted!`);
  };
}

let Squirtle = new createPokemon(`Squirtle`, 14, 50);
let Ratata = new createPokemon(`Ratata`, 2, 21);

console.log(Squirtle);
Squirtle.tackle(Ratata);
